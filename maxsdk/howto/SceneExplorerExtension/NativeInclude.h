// Copyright 2010 Autodesk, Inc.  All rights reserved.
//
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of Autodesk, Inc. and
// are protected under applicable copyright and trade secret law.  They may not
// be disclosed to, copied or used by any third party without the prior written
// consent of Autodesk, Inc.

#pragma once

// To avoid errors due to conflicting metadata definitions, native code #include
// directives are gathered in this file and included as a set.  That way, any
// metadata directives will be applied consistently across the assembly.

#pragma managed(push, off)
#include <windows.h>
#include <max.h>
#include <bmmlib.h>
#include <guplib.h>
#include <gup.h>
#include <noncopyable.h>
// allow methods taking an INode* parameter to be visible outside this assembly
#pragma make_public(INode)
#pragma managed(pop)
